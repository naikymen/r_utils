---
title: "Task name: Short title"
author: "Nicolás Méndez"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float:
      collapsed: false
    toc_depth: 4
    number_sections: false
    smooth_scroll: false
    code_folding: hide
    code_download: true
  pdf_document:
    latex_engine: xelatex
    toc: true
    toc_depth: 4
    number_sections: true
editor_options:
  chunk_output_type: inline
date: "`r format(Sys.time(), '%d %B, %Y')`"
urlcolor: blue
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "output/renders") })
# bibliography: references.bib
---

<!-- Templates are located within the inst/rmarkdown/templates directory of a package. -->

```{r setup, message=F}
knitr::opts_chunk$set(message = F)
knitr::opts_knit$set(root.dir = here::here())
# knitr::opts_chunk$set(cache = T)

# library(utiles)
# library(tidyverse)
# library(dplyr)
# library(tidyr)
# library(purrr)
# library(plotly)
# library(magick)

# library(renv)
```

```{r reticulate_setup}
library(reticulate)

# use_virtualenv("venv/", T)

# los plot no se mostraban :shrug: y con esto funcionó:
# https://community.rstudio.com/t/how-to-display-the-plot-in-the-python-chunk/22039/3
# https://github.com/rstudio/rstudio/blob/8aed5680540999b68d493c583c84bc2790ce4aff/src/cpp/session/modules/SessionReticulate.R#L163

# matplotlib <- reticulate::import("matplotlib")
# matplotlib$use("Agg", force = TRUE)
```


## About

<!-- context and details section -->

## Load data

```{r}

```

## Processing and analysis

```{r}

```

## Save results

```{r}

```

