library(flowCore)
library(flowViz)
setwd('/media/naikymen/SD64/Tesina/Resultados/Citometria/')
fs <- read.flowSet(path = "/media/naikymen/SD64/Tesina/Resultados/Citometria/", pattern = ".FCS")
sampleNames(fs)

f=fs[[13]]
plot(f, c("FSC", "SSC"), smooth=FALSE,log='xy',xlim=c(1,1000),ylim=c(1,1000),main='Wild type')  # x e y
abline(v = 500, col = "blue", lwd = 3, lty = "dashed")
abline(v = 20, col = "red", lwd = 3, lty = "dashed")
abline(h = 10, col = "yellow", lwd = 3, lty = "dashed")

# Conseguir los eventos con fluorescencia mayor a 500
margin.cells <- which(exprs(f)[, 'FSC'] >= 500)
lenM <- length(margin.cells)
len1 <- nrow(f)
margin.percent <- 100 * lenM/len1
margin.percent
# Y marcarlos con un punto rojo
A <- exprs(f)[,c("FSC","SSC")]
points(A[margin.cells, ], pch=".", col = 'red', cex = 2)
legend('top', legend = paste("Margin events:",margin.percent,"%"),col='red',pch=19)
# Antes de eliminar "basura" correr PBS solo para ver la basura normal y estar seguro.
# En especial porque los parásitos son chicos, las células de mamífero son mucho más grandes y no es necesario.

# Remover los eventos del margen y guardarlos
f.clean.margin <- f[-margin.cells]
lenM <- length(margin.cells); len1 <- nrow(f); margin.percent <- 100 * lenM/len1;
plot(f.clean.margin, c("FSC", "SSC"), smooth=FALSE,log='xy',xlim=c(1,1000),ylim=c(1,1000),main='Wild type')  # x e y
abline(v = 500, col = "blue", lwd = 3, lty = "dashed")
legend('top', legend = paste("Margin events:",margin.percent,"%"),col='red',pch=19)
